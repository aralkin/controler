const util = require('util');
const fs = require('fs');

const creator = util.promisify(fs.writeFile)

const createFile = async (file, text) => {
await creator(file, text).catch(err => console.log(err))
    console.log(`${file} created!`)
};



module.exports = createFile