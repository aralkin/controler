const util = require('util');
const fs = require('fs');

const renamer = util.promisify(fs.rename)

const renameFile = async (file,rename) => {
    await renamer(file, rename).catch(err => console.log(err))
    console.log(`${file} now is ${rename}!`)
};



module.exports = renameFile