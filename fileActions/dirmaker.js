const util = require('util');
const fs = require('fs');

const dirmaker = util.promisify(fs.mkdir)

const makeDir = async (dir) => {
    await dirmaker(dir, { recursive: true }).catch(err => console.log(err))
    console.log(`${dir} folder created!`)
};


module.exports = makeDir