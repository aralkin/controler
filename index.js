
const createFile = require('./fileActions/creator')
const readFile = require('./fileActions/reader')
const renameFile = require('./fileActions/renamer')
const removeFile = require('./fileActions/remover')
const makeDir = require('./fileActions/dirmaker')
const editFile = require('./fileActions/editor')
const {defaultMarkup} = require('./defaultMarkup')
const {styledList} = require('./defaultMarkup')

const fileSystem = async () => {
    await createFile('example.txt', 'hello body' );
    await readFile('example.txt');
    await renameFile('example.txt', 'new-example.txt');
    await removeFile('new-example.txt');
    await makeDir('./src');
    await makeDir('./src/html');
    await createFile('./src/html/index.html', defaultMarkup);
    await editFile('./src/html/index.html', '<body>', styledList);
    await makeDir('./build');
    await makeDir('./build/html');
    await renameFile('./src/html/index.html', './build/html/index.html');

}

fileSystem()