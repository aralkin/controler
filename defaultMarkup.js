const defaultMarkup = `<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8" />
  <title>HTML5</title>
  <style>
   article, aside, details, figcaption, figure, footer,header,
   hgroup, menu, nav, section { display: block; }
  </style>
 </head>
 <body>
  <p>Привет, мир</p>
 </body>
</html>`

const styledList = `
<body>
<ul style="list-style-type:square;">
    <li>First movie</li>
    <li>Last movie</li>
</ul>
` 

module.exports.defaultMarkup = defaultMarkup
module.exports.styledList = styledList