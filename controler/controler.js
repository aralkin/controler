const { promisify } = require('util');
const uuidv1 = require('uuidv1');
const fs = require('fs');

const readFile = promisify(fs.readFile);
const isFileAccessible = promisify(fs.access);
const writeFile = promisify(fs.writeFile);

const familySchema = {
    "familyGroups": [
        { "group": "not choosed", "id": 1 },
        { "group": "parents", "id": 2 },
        { "group": "childrens", "id": 3 },
        { "group": "pets", "id": 4 },
    ],
    "familyMembers": [],
};

class Family {
    constructor(fileName) {
        this.fileName = fileName;
    }

    async getFileContent() {
        const readedFile = await readFile(this.fileName, 'utf-8');
        const parsedFile = JSON.parse(readedFile);
        this.family = parsedFile

    }


    async fileExistenseCheck() {
        try {
            await isFileAccessible(this.fileName)
        }
        catch (err) {
            if (err) {
                await writeFile(this.fileName, JSON.stringify(familySchema, null, 2));
            }
        }
    }

    async fileValidation() {
        try {
            const isValid = /(?=.*familyGroup)(?=.*familyMembers)/gi.test(JSON.stringify(this.family));
            if (isValid) return;
            else {
                throw `File isn't valid!`
            }
        } catch (err) {
            console.log(err)
        }
    }


    async init() {
        await this.fileExistenseCheck();
        await this.getFileContent();
        await this.fileValidation()
    };


    findMember (newMember) {
        return this.family.familyMembers.find(member => member.name === newMember.name && member.lastName === newMember.lastName)
    };

    memberExistenseCheck (newMember){
        const familyMember = this.findMember(newMember)
        if (!familyMember) {
            throw ` ${newMember.name} ${newMember.lastName} is doesn't exist`
        }
        return familyMember;
    };



    findMembersGroup(group) {
        return this.family.familyGroups.find(item => item.group === group);
    };


    validateMember(newMember) {
        const isMemberExist = this.findMember(newMember);
        if (isMemberExist) throw 'User already exist';
        const isGroupExist = this.findMembersGroup(newMember.group);
        if (!isGroupExist) throw 'Wrong group';
        return newMember;
    };


    validateMemberSchema(newMember) {
        const isMemberKeysValid = newMember.hasOwnProperty("name", "lastName", "group", "favorite", "owner")
        if (!isMemberKeysValid) { throw 'invalid member keys!' }
    };



    createMemberObject(newMember) {

        const memberObject = {};

        memberObject.name = newMember.name;
        memberObject.lastName = newMember.lastName;
        memberObject.group = this.findMembersGroup(newMember.group).id;
        if (newMember.favorite.length > 0 ) {
            memberObject.favorites =  newMember.favorite.map(user => this.memberExistenseCheck(user).id);
        }
        if (newMember.owner && newMember.owner.name)
        memberObject.owner =  this.memberExistenseCheck(newMember.owner).id;
        memberObject.id = uuidv1();

        return memberObject;
    }



    async addFamilyMember(newMember) {
        await this.init()
        this.validateMemberSchema(newMember);
        this.validateMember(newMember)

        const memberObject = this.createMemberObject(newMember);

        this.family.familyMembers.push(memberObject)
        const stringifyFamilyMembers = JSON.stringify(this.family, null, 2);
        writeFile(this.fileName, stringifyFamilyMembers, 'utf-8');
    };
}

const myFamily = new Family('./family.json');

myFamily.addFamilyMember({ name: 'Dima', lastName: 'Shlyapa', group: 'childrens', favorite: [ { name: 'Liza', lastName: 'Shlyapa' }], owner: { name: 'Liza', lastName: 'Shlyapa' } }),

    module.exports = Family;
